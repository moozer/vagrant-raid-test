#!/usr/bin/env bash


echo "count;normal;raid5;raid1;raid0"
for I in $(seq 10); do
        echo -n "$I;"
        sudo hdparm -t /dev/vdb /dev/md0 /dev/md1 /dev/md2 | grep Timing | awk '{printf "%.0f;", $11 }'
        echo ""
done
