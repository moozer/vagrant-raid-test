RAID test
==================

We test RAID0, 1 and 5 by creating a VM with 10 disks
* 1 for OS
* 4 for RAID5
* 2 for RAID1
* 3 for RAID0

Banchmark result is saved so `benchmarks.txt"

ref
* https://www.tutorialspoint.com/how-to-create-a-raid-5-storage-array-with-mdadm-on-ubuntu-16-04